<?php namespace mef\Orm\Test;

use PHPUnit\Framework\TestCase;

class SqlManagerTest extends TestCase
{
	public function setup() : void
	{
		$this->orm = new Orm;
	}

	public function testAutoInsert()
	{
		$user = $this->orm->create('user', ['first_name' => 'Matthew']);
		$user->save();

		$this->assertSame(1, $user->id);
	}

	public function testNoAutoInsert()
	{
		$token = $this->orm->create('token', ['id' => 'abc123']);
		$token->save();

		$this->assertSame('abc123', $token->id);
	}
}