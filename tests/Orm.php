<?php namespace mef\Orm\Test;

use PDO;
use mef\Db\Driver\PdoDriver;
use mef\Sql\Writer\SqliteWriter;
use mef\Sql\Driver\SqlDriver;
use mef\Orm\SQLManager;
use mef\Orm\Entity;
use mef\Orm\Integer;
use mef\Orm\Text;
use mef\Orm\FloatField;

class Orm extends SQLManager
{
	public function __construct()
	{
		$pdo = new PDO('sqlite::memory:');
		$pdoDriver = new PdoDriver($pdo);
		$writer = new SqliteWriter($pdoDriver);
		$db = new SqlDriver($writer);
		$db->execute('CREATE TABLE user (id INTEGER PRIMARY KEY, first_name TEXT, last_name TEXT, password TEXT)');
		$db->execute('CREATE TABLE token (id TEXT PRIMARY KEY)');

		parent::__construct($db);
		$this->lastInsertFunction = 'LAST_INSERT_ROWID';

		$entity = new Entity('user');
		$entity['id'] = new Integer('id');
		$entity['first_name'] = new Text('first_name');
		$entity['last_name'] = new Text('last_name');
		$entity['password'] = new Text('password');
		$entity['latitude'] = new FloatField('latitude');
		$entity['longitude'] = new FloatField('longitude');
		$this->mapEntity($entity, 'user');

		$entity = new Entity('token');
		$entity['id'] = new Text('id');
		$this->mapEntity($entity, 'token');
	}
}