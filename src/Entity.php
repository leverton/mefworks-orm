<?php

namespace mef\Orm;

use ArrayAccess;

class Entity implements ArrayAccess
{
    use Getter;

    protected array $fields = [];

    public function __construct(protected string $name)
    {
    }

    final public function __getName(): string
    {
        return $this->name;
    }

    final public function __getFields(): array
    {
        return $this->fields;
    }

    public function offsetExists(mixed $name): bool
    {
        return array_key_exists($name, $this->fields);
    }

    public function offsetGet(mixed $name): mixed
    {
        return $this->fields[$name];
    }

    public function offsetSet(mixed $name, mixed $value): void
    {
        if (!$name) {
            $name = $value->name;
        }

        $this->fields[$name] = $value;
    }

    public function offsetUnset(mixed $name): void
    {
        unset($this->fields[$name]);
    }
}
