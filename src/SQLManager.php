<?php

namespace mef\Orm;

use Serializable;
use mef\Sql\Driver\SqlDriverInterface;

class SQLManager extends Manager implements Serializable
{
    protected string $lastInsertFunction = 'LAST_INSERT_ID';

    public function __construct(protected SqlDriverInterface $db)
    {
    }

    protected function __getDb(): SqlDriverInterface
    {
        return $this->db;
    }

    protected function __setDb(SqlDriverInterface $db): void
    {
        $this->db = $db;
    }

    protected function __getLastInsertFunction(): string
    {
        return $this->lastInsertFunction;
    }

    protected function __setLastInsertFunction(string $functionName)
    {
        $this->lastInsertFunction = $functionName;
    }

    public function get(string $model_name, mixed $id): ?Model
    {
        $entity = $this->entities[$model_name];
        $values = $this->db->select()->from($entity->name)->where('id', $id)->limit(1)->query()->fetchRow();
        if (!$values) {
            return null;
        }

        return $this->createInitializedWithData($model_name, $values);
    }

    public function insert(string $model_name, array $values): mixed
    {
        $entity = $this->entities[$model_name];
        $values = array_intersect_key($values, $entity->fields);

        $this->db->insert()->into($entity->name)->fields(array_keys($values))->values($values)->execute();

        return isset($values['id']) === true ? $values['id'] : $this->db->query('SELECT ' . $this->lastInsertFunction . '()')->fetchValue();
    }

    public function update(string $model_name, array $values, mixed $id): void
    {
        $entity = $this->entities[$model_name];
        $values = array_intersect_key($values, $entity->fields);

        $this->db->update()->table($entity->name)->namedValues($values)->where('id', $id[0])->execute();
    }


    public function doDelete(string $model_name, mixed $id): void
    {
        $entity = $this->entities[$model_name];

        $this->db->delete()->from($entity->name)->where('id', $id)->execute();
    }

    /**
     * Return serialized data.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize($this->__serialize);
    }

    /**
     * Returns the data as an array.
     *
     * @return array
     */
    public function __serialize(): array
    {
        return [
            'entities' => $this->entities,
            'entity_map' => $this->entity_map,
            'relationships' => $this->relationships,
            'lastInsertFunction' => 'LAST_INSERT_ID',
        ];
    }

    /**
     * Unserialize the data and set the appropriate member variables.
     *
     * @param  string $serializedString
     */
    public function unserialize($serializedString)
    {
        $data = unserialize($serializedString);

        $this->__unserialize($data);
    }

    /**
     * Take the data and set the appropriate member variables.
     *
     * @param  array $serializedString
     */
    public function __unserialize(array $data): void
    {
        $this->entities = $data['entities'];
        $this->entity_map = $data['entity_map'];
        $this->relationships = $data['relationships'];
        $this->lastInsertFunction = $data['lastInsertFunction'];
    }
}
