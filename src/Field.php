<?php

namespace mef\Orm;

abstract class Field
{
    use Getter;
    use Setter;

    public function __construct(protected string $name)
    {
        $this->name = $name;
    }

    public function sanitize(mixed $value): mixed
    {
        return $value;
    }

    protected function __setName(string $name)
    {
        $this->name = $name;
    }

    public function __getName(): string
    {
        return $this->name;
    }
}
