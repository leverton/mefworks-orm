<?php

namespace mef\Orm;

use InvalidArgumentException;

class LinkedModel
{
    protected ?Model $model;

    public function __construct(
        protected Manager $manager,
        protected ActiveRecord $record,
        protected string $pk,
        protected string $entity,
        protected string $fk
    ) {
    }

    public function get(): ?Model
    {
        if ($this->model === null) {
            $this->model = $this->manager->query($this->entity)->
                where($this->pk, $this->record[$this->fk])->
                findOne();
        }

        return $this->model;
    }

    public function set(Model $value): void
    {
        $type = $this->manager->getEntityModelName($this->entity);
        if (!$value instanceof $type) {
            throw new InvalidArgumentException();
        }

        $this->model = $value;
        $this->record[$this->fk] = $value[$this->pk];
    }

    public function isLoaded(): bool
    {
        return $this->model !== null;
    }
}
