<?php

namespace mef\Orm;

class Text extends Field
{
    protected ?array $values = null;
    protected ?int $max_length;

    public function __construct(string $name, ?int $max_length = null, ?array $values = null)
    {
        parent::__construct($name);

        $this->max_length = $max_length;

        if (is_array($values)) {
            $this->values = array_flip($values);
        }
    }

    public function sanitize(mixed $value): ?string
    {
        if ($value !== null) {
            $value = (string) $value;

            if ($this->max_length !== null && $value !== '') {
//              $value = substr($value, 0, $this->max_length);
            }
        }

        return !$this->values || isset($this->values[$value]) ? $value : null;
    }
}
