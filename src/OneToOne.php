<?php

namespace mef\Orm;

class OneToOne extends Relationship
{
    use Getter;
    use Setter;

    public function __construct(RelatedEntity $a, RelatedEntity $b)
    {
        $this->__a = $a;
        $this->__b = $b;
    }

    protected RelatedEntity $__a;

    protected function __getA(): RelatedEntity
    {
        return $this->__a;
    }

    protected function __setA(array|RelatedEntity $a): void
    {
        $this->__a = RelatedEntity::cast($a);
    }

    protected RelatedEntity $__b;

    protected function __getB(): RelatedEntity
    {
        return $this->__b;
    }

    protected function __setB(array|RelatedEntity $b): void
    {
        $this->__b = RelatedEntity::cast($b);
    }

    public function reverse(): static
    {
        return new self($this->__b, $this->__a);
    }
}
