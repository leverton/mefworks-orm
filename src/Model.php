<?php

namespace mef\Orm;

abstract class Model
{
    protected Manager $_manager;
    protected string $_name;
    protected bool $_attached = false;

    final public function __construct(Manager $manager, string $name, array $data = [])
    {
        $this->_manager = $manager;
        $this->_name = $name;
        $this->init($data);
    }

    public function isAttached(): bool
    {
        return $this->_attached;
    }

    public function attach(): void
    {
        $this->_attached = true;
    }

    public function detach(): void
    {
        $this->_attached = false;
    }

    public function getEntityName(): string
    {
        return $this->_name;
    }

    abstract protected function init(array $data = []): void;
    abstract public function load(array $values): void;
    abstract public function save(): void;
}
