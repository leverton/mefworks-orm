<?php

namespace mef\Orm;

use mef\Validation\Cast;

class RelatedEntity implements Cast
{
    use Getter;
    use Setter;

    protected string $__entity, $__key;
    protected ?string $__name;

    public function __construct(string $entity, string $key, string $name = null)
    {
        $this->entity = $entity;
        $this->key = $key;
        $this->name = $name;
    }

    public static function cast(mixed $obj): RelatedEntity
    {
        if ($obj instanceof self) {
            return $obj;
        } elseif (is_array($obj) && !empty($obj['entity']) && !empty($obj['key'])) {
            $cast = new static($obj['entity'], $obj['key']);
            if (isset($obj['name'])) {
                $cast->name = $obj['name'];
            }

            return $cast;
        }
    }

    protected function __getEntity(): string
    {
        return $this->__entity;
    }

    protected function __setEntity(string $entity): void
    {
        $this->__entity = $entity;
    }

    protected function __getKey(): string
    {
        return $this->__key;
    }

    protected function __setKey(string $key): void
    {
        $this->__key = $key;
    }

    protected function __getName(): ?string
    {
        return $this->__name;
    }

    protected function __setName(?string $name): void
    {
        $this->__name = $name;
    }
}
