<?php

namespace mef\Orm;

/**
 * Provides writable attributes by replacing the PHP magic __set with a
 * function per property.
 *
 * For example, to enforce an integer property:
 *
 * <code>
 * class Foo
 * {
 *   private $__bar;
 *
 *   private function __setBar($bar)
 *   {
 *     $this->__bar = (int) $bar;
 *   }
 * }
 *
 * $foo = new Foo();
 * $foo->bar = '42';
 * </code>
 *
 * @todo Remove legacy support for setFoo (no underscores)
 */
trait Setter
{
    final public function __set(mixed $name, mixed $value): void
    {
        $method = '__set' . $name;
        if (is_callable([$this, $method])) {
            $this->{$method}($value);
        } else {
            $this->{'set' . $name}($value);
        }
    }
}
