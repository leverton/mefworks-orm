<?php

namespace mef\Orm;

use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use Serializable;
use Traversable;

class LinkedModelList implements IteratorAggregate, Countable
{
    use Getter;

    protected array $models;

    protected array $updated = [];

    public function __construct(protected Manager $manager, protected ActiveRecord $record, string $pk, string $entity, string $fk)
    {
    }

    public function query(): Query
    {
        $query = new Query($this->manager, $this->manager->entities[$this->entity]);
        $query->where($this->fk, $this->record[$this->pk]);

        return $query;
    }

    public function count(): int
    {
        if ($this->models === null) {
            $this->fetch();
        }

        return count($this->models);
    }

    public function add(ActiveRecord $record): void
    {
        if (get_class($record) != $this->entity) {
            throw new InvalidArgumentException();
        }

        $record[$this->fk] = $this->record[$this->pk];

        $this->updated[] = $record;
        $this->models[] = $record;
    }

    private function __getFetched(): bool
    {
        return $this->models !== null;
    }

    private function __getStale(): bool
    {
        return $this->updated ? true : false;
    }

    public function fetch(): static
    {
        $this->models = $this->query()->find();
        return $this;
    }

    public function save(): void
    {
        foreach ($this->updated as $record) {
            $record[$this->fk] = $this->record[$this->pk];
            $record->save();
        }

        $this->updated = [];
    }

    public function getIterator(): ArrayIterator
    {
        if ($this->models === null) {
            $this->fetch();
        }

        return new ArrayIterator($this->models);
    }

    public function __serialize(): array
    {
        return [
            'pk' => $this->pk,
            'entity' => $this->entity,
            'fk' => $this->fk,
        ];
    }

    public function __unserialize(array $data): void
    {
        $this->pk = $data['pk'];
        $this->entity = $data['entity'];
        $this->fk = $data['fk'];
    }
}
