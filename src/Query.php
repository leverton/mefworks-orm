<?php

namespace mef\Orm;

use mef\Sql\Builder\SelectBuilder;
use mef\Sql\OrderBy;

class Query
{
    protected int $offset = 0;

    use \mef\Sql\WhereTrait;
    use \mef\Sql\LimitTrait;
    use Getter;

    public function __construct(protected Manager $manager, protected Entity $entity)
    {
        $this->__whereClause = new Where($this);
        $this->__orderBy = new OrderBy($this);
    }

    private function attachWhereClause(SelectBuilder $q): void
    {
        $q->setWhereClause($this->__whereClause);
    }

    public function find(): array
    {
        $models = [];
        $model_name = $this->manager->getEntityModelName($this->entity->name);

        $q = $this->manager->db->select()->from($this->entity->name);
        $this->attachWhereClause($q);

        foreach ($this->orderBy as $order) {
            $q->orderBy($order->field, $order->direction);
        }

        if ($this->offset) {
            $q->offset($this->offset);
        }

        if ($this->limit) {
            $q->limit($this->limit);
        }

        $reflector = new \ReflectionClass($model_name);

        foreach ($q->query() as $row) {
            $model = $reflector->newInstance($this->manager, $this->entity->name, $row);
            $models[] = $model;
        }

        return $models;
    }

    public function offset(int $offset): static
    {
        $this->offset = $offset;
        return $this;
    }

    public function count(): int
    {
        $q = $this->manager->db->select()->from($this->entity->name);
        $this->attachWhereClause($q);

        foreach ($this->orderBy as $order) {
            $q->orderBy($order->field, $order->direction);
        }

        if ($this->limit) {
            $q->limit($this->limit);
        }

        return $q->count();
    }

    public function findOne(): ?Model
    {
        $this->limit = 1;
        foreach ($this->find() as $row) {
            return $row;
        }

        return null;
    }

    public function delete(): void
    {
        $q = $this->manager->db->delete()->from($this->entity->name);
        $this->attachWhereClause($q);

        $q->exec();
    }

    protected OrderBy $__orderBy;

    final protected function __getOrderBy(): OrderBy
    {
        return $this->__orderBy;
    }

    public function orderBy(string $field, string $direction = 'ASC'): static
    {
        $this->__orderBy->__invoke($field, $direction);
        return $this;
    }
}
