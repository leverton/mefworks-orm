<?php

namespace mef\Orm;

class OneToMany extends Relationship
{
    use Getter;
    use Setter;

    protected RelatedEntity $__parent;

    protected function __getParent(): RelatedEntity
    {
        return $this->__parent;
    }

    protected function __setParent(array|RelatedEntity $parent): void
    {
        $this->__parent = RelatedEntity::cast($parent);
    }

    protected RelatedEntity $__child;

    protected function __getChild(): RelatedEntity
    {
        return $this->__child;
    }

    protected function __setChild(array|RelatedEntity $child)
    {
        $this->__child = RelatedEntity::cast($child);
    }
}
