<?php

namespace mef\Orm;

use ArrayAccess;
use Exception;

class ActiveRecord extends Model implements ArrayAccess
{
    private static array $modifiers = [];
    private string $model_name;

    protected Entity $_entity;
    protected array $_values;
    protected array $_stale = [];
    protected array $_id = [];

    protected function init(array $defaults = []): void
    {
        $this->model_name = $this->_name;
        $this->_entity = $this->_manager->getEntity($this->_name);
        $field_names = array_keys($this->_entity->fields);
        $this->_values = array_fill_keys($field_names, null);

        foreach ($this->_manager->relationships[$this->model_name] as $relationship) {
            if ($relationship instanceof OneToMany) {
                $this->_values[$relationship->child->name] = new LinkedModelList(
                    $this->_manager,
                    $this,
                    $relationship->parent->key,
                    $relationship->child->entity,
                    $relationship->child->key
                );
            } elseif ($relationship instanceof ManyToOne) {
                $this->_values[$relationship->parent->name] = new LinkedModel(
                    $this->_manager,
                    $this,
                    $relationship->parent->key,
                    $relationship->parent->entity,
                    $relationship->child->key
                );
            } elseif ($relationship instanceof OneToOne) {
                $this->_values[$relationship->b->name] = new LinkedModel(
                    $this->_manager,
                    $this,
                    $relationship->b->key,
                    $relationship->b->entity,
                    $relationship->a->key
                );
            }
        }

        foreach (array_intersect_key($defaults, $this->_values) as $name => $value) {
            $this->_values[$name] = $value;
        }

        # TODO: query entity to determine primary key
        if (isset($defaults['id'])) {
            $this->_id = [$defaults['id']];
        }

        if (!array_key_exists($this->model_name, self::$modifiers)) {
            $funcs = [];
            foreach ($field_names as $field_name) {
                $func = [$this, 'transform' . str_replace('_', '', $field_name)];
                if (is_callable($func)) {
                    $funcs[$field_name] = $func;
                }
            }
            self::$modifiers[$this->model_name] = $funcs;
        }
    }

    public function offsetExists(mixed $name): bool
    {
        return array_key_exists($name, $this->_values);
    }

    public function offsetGet(mixed $name): mixed
    {
        return $this->get($name);
    }

    public function offsetSet(mixed $name, mixed $value): void
    {
        $this->set($name, $value);
    }

    public function offsetUnset(mixed $name): void
    {
        $this->values[$name] = null;
    }

    public function __get(mixed $name): mixed
    {
        return $this->get($name);
    }

    public function __set(mixed $name, mixed $value): void
    {
        $this->set($name, $value);
    }

    protected function get(mixed $name): mixed
    {
        if (!array_key_exists($name, $this->_values)) {
            throw new Exception("Invalid property $name");
        }

        if ($this->_values[$name] instanceof LinkedModel) {
            return $this->_values[$name]->get();
        } else {
            return $this->_values[$name];
        }
    }

    protected function set(mixed $name, mixed $value): void
    {
        if (!array_key_exists($name, $this->_values)) {
            throw new Exception("Invalid property $name");
        }

        if (array_key_exists($name, self::$modifiers[$this->model_name])) {
            $value = call_user_func(self::$modifiers[$this->model_name][$name], $value);
        }

        if ($this->_values[$name] instanceof LinkedModel) {
            $this->_values[$name]->set($value);
        } else {
            $this->_values[$name] = $this->_entity->fields[$name]->sanitize($value);
        }

        $this->_stale[$name] = true;
    }

    public function load(array $values): void
    {
        $values = array_intersect_key($values, $this->_values);
        foreach ($values as $name => $value) {
            $this->set($name, $value);
        }
    }

    public function reset(): void
    {
        $this->_id = [];
        $this->_stale = [];
        $this->_values = array_fill_keys(array_keys($this->_entity->fields), null);
    }

    public function save(): void
    {
        $stale_links = [];
        foreach ($this->_values as $name => $value) {
            if ($value instanceof LinkedModelList) {
                if ($value->stale) {
                    $stale_links[] = $value;
                }
            }
        }

        if ($this->_stale) {
            if (!$this->_id) {
                $id = $this->_manager->insert($this->_name, array_intersect_key($this->_values, $this->_stale));
                if ($id) {
                    $this->set('id', $id);
                    $this->_id = [$id];
                }
                $this->_stale = [];
            } else {
                $this->_manager->update($this->_name, array_intersect_key($this->_values, $this->_stale), $this->_id);
            }
        }

        foreach ($stale_links as $stale_link) {
            $stale_link->save();
        }
    }

    public function delete(): void
    {
        $this->_manager->delete($this);
    }

    public function asArray(): array
    {
        return $this->_values;
    }
}
