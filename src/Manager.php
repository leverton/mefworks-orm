<?php

namespace mef\Orm;

use ArrayAccess;
use Exception;

abstract class Manager implements ArrayAccess
{
    use Getter;
    use Setter;

    protected array $entities = [];
    protected array $entity_map = [];
    protected array $relationships = [];

    public function __construct()
    {
    }

    protected function __getEntities(): array
    {
        return $this->entities;
    }

    protected function __getRelationships(): array
    {
        return $this->relationships;
    }

    public function offsetExists(mixed $name): bool
    {
        return isset($this->entities[$name]);
    }

    public function offsetGet(mixed $name): mixed
    {
        return $this->entities[$name];
    }

    public function offsetSet(mixed $name, mixed $value): void
    {
        throw new Exception("Read Only Property");
    }

    public function offsetUnset(mixed $name): void
    {
        throw new Exception("Read Only Property");
    }

    public function mapEntity(Entity $entity, string $name, string $model_name = 'mef\Orm\ActiveRecord')
    {
        $this->entities[$name] = $entity;
        $this->entity_map[$name] = $model_name;
        $this->relationships[$name] = [];
    }

    public function getEntity(string $model_name): Entity
    {
        return $this->entities[$model_name];
    }

    public function getEntityModelName(string $entity_name): ?string
    {
        return array_key_exists($entity_name, $this->entity_map) ? $this->entity_map[$entity_name] : null;
    }

    public function addRelationship(Relationship $relationship): void
    {
        if ($relationship instanceof OneToMany) {
            if ($relationship->child->name) {
                $this->relationships[$relationship->parent->entity][] = $relationship;
            }

            if ($relationship->parent->name) {
                $this->relationships[$relationship->child->entity][] = ManyToOne::cast($relationship);
            }
        } elseif ($relationship instanceof ManyToOne) {
            if ($relationship->child->name) {
                $this->relationships[$relationship->parent->entity][] = OneToMany::cast($relationship);
            }

            if ($relationship->parent->name) {
                $this->relationships[$relationship->child->entity][] = $relationship;
            }
        } elseif ($relationship instanceof OneToOne) {
            if ($relationship->a->name) {
                $this->relationships[$relationship->b->entity][] = $relationship->reverse();
            }

            if ($relationship->b->name) {
                $this->relationships[$relationship->a->entity][] = $relationship;
            }
        }
    }

    public function create(string $entity, array $data = []): Model
    {
        $model_name = $this->entity_map[$entity];
        $model = new $model_name($this, $entity);

        if ($data) {
            $model->load($data);
        }
        return $model;
    }

    /**
     * Creates an entity and initializes it with the given data. This is
     * currently used to provide a way to differentiate between models that
     * need to be inserted and those that need to be updated. Per the
     * current implementation of ActiveRecord, using this method will cause
     * the model to be updated.
     *
     * @param string $entity   The entity name
     *
     * @return mef\ORM\Model
     */
    protected function createInitializedWithData(string $entity, array $data): Model
    {
        $model_name = $this->entity_map[$entity];
        return new $model_name($this, $entity, $data);
    }

    public function query(string $model_name): Query
    {
        return new Query($this, $this->entities[$model_name]);
    }

    public function save(Model $model): void
    {
        if (!$model->isAttached()) {
            $id = $this->insert(
                $model->getEntityName(),
                $model->asArray()
            );

            $model->id = $id;
            $model->attach();
        } else {
            $this->update(
                $model->getEntityName(),
                $model->asArray(),
                $model->id
            );
        }
    }

    public function delete(Model $model): void
    {
        $this->doDelete(
            $model->getEntityName(),
            $model->id
        );
    }

    abstract public function get(string $model_name, mixed $id): ?Model;

    abstract public function insert(string $model_name, array $values): mixed;

    abstract public function update(string $model_name, array $values, mixed $id): void;

    abstract protected function doDelete(string $model_name, mixed $id): void;
}
