<?php

namespace mef\Orm;

use MongoDB;
use MongoId;

class MongoManager extends Manager
{
    public function __construct(pongoDB $db)
    {
        $this->_db = $db;
    }

    /**
     * @var $db mongodb
     */
    protected MongoDb $_db;

    final protected function __getDb(): MongoDb
    {
        return $this->_db;
    }

    protected function __setDb(MongoDB $db): void
    {
        $this->_db = $db;
    }

    public function get(string $model_name, mixed $id): Model
    {
        if (!($id instanceof MongoId)) {
            $id = new MongoId($id);
        }

        $entity = $this->entities[$model_name];
        $mongo_values = $this->db->selectCollection($entity->name)->findOne(['_id' => $id]);

        if (!$mongo_values) {
            return null;
        }

        $map = array_flip(array_map(function ($f) {
            return $f->name;
        }, $entity->fields));

        $values = [];
        foreach ($mongo_values as $key => $mongo_value) {
            if ($mongo_value instanceof MongoId) {
                $mongo_value = (string) $mongo_value;
            }

            if (isset($map[$key])) {
                $values[$map[$key]] = $mongo_value;
            }
        }

        $model = $this->create($model_name, $values);
        $model->attach();

        return $model;
    }

    public function insert(string $model_name, array $values): string
    {
        $entity = $this->entities[$model_name];

        $mongo_values = [];
        foreach (
            array_map(function ($f) {
                return $f->name;
            }, $entity->fields) as $alias => $key
        ) {
            if (isset($values[$alias])) {
                $mongo_values[$key] = $values[$alias];
            }
        }

        $rv = $this->_db->selectCollection($entity->name)->insert($mongo_values, ['safe' => true]);

        return (string) $mongo_values['_id'];
    }

    public function update(string $model_name, array $values, mixed $id): void
    {
        if (!($id instanceof MongoId)) {
            $id = new MongoId($id);
        }

        $entity = $this->entities[$model_name];

        $mongo_values = [];
        foreach (
            array_map(function ($f) {
                return $f->name;
            }, $entity->fields) as $alias => $key
        ) {
            if (isset($values[$alias])) {
                $mongo_values[$key] = $values[$alias];
            }
        }

        unset($mongo_values['_id']);

        $rv = $this->_db->selectCollection($entity->name)->update(
            ['_id' => $id],
            ['$set' => $mongo_values],
            ['safe' => true]
        );

        // return $rv['err'] == null;
    }


    protected function doDelete(string $model_name, mixed $id): void
    {
        if (!($id instanceof MongoId)) {
            $id = new MongoId($id);
        }

        $entity = $this->entities[$model_name];

        $rv = $this->_db->selectCollection($entity->name)->remove(
            ['_id' => $id],
            ['safe' => true, 'justOne' => true]
        );

        // return $rv['error'] == null;
    }
}
