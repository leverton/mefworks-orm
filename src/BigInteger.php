<?php

namespace mef\Orm;

use GMP;

class BigInteger extends Field
{
    protected GMP $min, $max;

    public function __construct(string|int $min = null, string|int $max = null)
    {
        $this->min = gmp_init($min);
        $this->max = gmp_init($max);
    }

    public function sanitize(mixed $value): mixed
    {
        if (is_float($value)) {
            $value = sprintf("%f", $value);
        }

        if ($value === null || !preg_match('/^\s*([+-]?\d+)/', $value, $m)) {
            return null;
        }

        $value = $m[1];

        if ($this->min !== null || $this->max !== null) {
            $gvalue = gmp_init($value);

            if ($this->min !== null && gmp_cmp($gvalue, $this->min) < 0) {
                return gmp_strval($this->min);
            } elseif ($this->max !== null && gmp_cmp($gvalue, $this->max) > 0) {
                return gmp_strval($this->max);
            } else {
                return $value;
            }
        }
    }
}
