<?php

namespace mef\Orm;

use DateTime as PhpDateTime;

class DateTime extends Field
{
    public function __construct(string $name, protected string $format)
    {
        parent::__construct($name);
    }

    public function sanitize(mixed $value): ?string
    {
        if ($value === null) {
            return null;
        }

        if (!$value instanceof PhpDateTime) {
            $value = new PhpDateTime($value);
        }

        return $value->format($this->format);
    }
}
