<?php

namespace mef\Orm;

class Integer extends Field
{
    public function __construct(string $name, protected ?int $min = null, protected ?int $max = null)
    {
        parent::__construct($name);
    }

    public function sanitize(mixed $value): ?int
    {
        if ($value === null || !preg_match('/^\s*([+-]?\d+)/', $value, $m)) {
            return null;
        }

        $value = (int) $m[1];

        if ($this->min !== null && $value < $this->min) {
            return $this->min;
        } elseif ($this->max !== null && $value > $this->max) {
            return $this->max;
        } else {
            return $value;
        }
    }
}
