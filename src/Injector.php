<?php

namespace mef\Orm;

trait ManagerInjector
{
    protected Manager $orm;

    public function setORM(Manager $orm)
    {
        $this->orm = $orm;
    }

    public function getORM(): Manager
    {
        return $this->orm;
    }
}
