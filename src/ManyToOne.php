<?php

namespace mef\Orm;

use mef\Validation\Cast;

class ManyToOne extends Relationship implements Cast
{
    use Getter;
    use Setter;

    protected RelatedEntity $parent, $child;

    protected function __getParent(): RelatedEntity
    {
        return $this->parent;
    }

    protected function __getChild(): RelatedEntity
    {
        return $this->child;
    }

    public static function cast(mixed $obj): static
    {
        if ($obj instanceof self) {
            return $obj;
        } elseif ($obj instanceof OneToMany) {
            $cast = new self();
            $cast->__setParent($obj->parent);
            $cast->__setChild($obj->child);
            return $cast;
        }
    }

    protected function __setParent(array|RelatedEntity $parent)
    {
        $this->parent = RelatedEntity::cast($parent);
    }

    protected function __setChild(array|RelatedEntity $child)
    {
        $this->child = RelatedEntity::cast($child);
    }
}
