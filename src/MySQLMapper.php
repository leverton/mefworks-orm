<?php

namespace mef\Orm;

use Exception;
use mef\Sql\Driver\SqlDriver;

class MySQLMapper
{
    public function __construct(protected SqlDriver $db)
    {
    }

    public function mapTable(string $table): Entity
    {
        $entity = new Entity($table);

        foreach ($this->db->query('DESCRIBE `' . $table . '`') as $row) {
            $row = (object) $row;

            if (!preg_match('/^([a-z]+)/', $row->Type, $m)) {
                throw new Exception("Unexpected format: {$row->Type}");
            }

            switch ($m[1]) {
                case 'bigint':
                    $field = (strpos($row->Type, 'unsigned') !== false) ?
                        new BigInteger($row->Field, '0', '18446744073709551615') : new Integer($row->Field, -9223372036854775808, 9223372036854775807);
                    break;

                case 'binary':
                    if (!preg_match('/\((\d+)\)/', $row->Type, $n)) {
                        throw new Exception("Unexpected format: {$row->Type}");
                    }
                    $max_length = $n[1];
                    $field = new Blob($row->Field);
                    break;

                case 'bit':
                    $field = new Boolean($row->Field);
                    break;

                case 'bool':
                case 'boolean':
                    $field = new Boolean($row->Field);
                    break;

                case 'char':
                    if (!preg_match('/\((\d+)\)/', $row->Type, $n)) {
                        throw new Exception("Unexpected format: {$row->Type}");
                    }
                    $field = new Text($row->Field, $n[1]);
                    break;

                case 'date':
                    $field = new DateTime($row->Field, 'Y-m-d');
                    break;

                case 'datetime':
                    $field = new DateTime($row->Field, 'Y-m-d H:i:s');
                    break;

                case 'decimal':
                    if (!preg_match('/\((\d+),(\d+)\)/', $row->Type, $n)) {
                        throw new Exception("Unexpected format: {$row->Type}");
                    }
                    list(, $size, $decimals) = $n;
                    $field = new Fixed($row->Field);
                    break;

                case 'double':
                    $field = new FloatField($row->Field);
                    break;

                case 'enum':
                    $options = str_getcsv(substr($row->Type, 5, -1), ',', "'");
                    $field = new Text($row->Field, null, $options ? $options : null);
                    break;

                case 'float':
                    $field = new FloatField($row->Field);
                    break;

                case 'int':
                    $field = (strpos($row->Type, 'unsigned') !== false) ?
                        new Integer($row->Field, 0, 4294967295) : new Integer($row->Field, -2147483648, 2147483647);
                    break;

                case 'longtext':
                    $field = new Text($row->Field);
                    break;

                case 'mediumint':
                    $field = (strpos($row->Type, 'unsigned') !== false) ?
                        new Integer($row->Field, 0, 16777215) : new Integer($row->Field, -8388608, 8388607);
                    break;

                case 'mediumtext':
                    $field = new Text($row->Field);
                    break;

                case 'set':
                    $options = str_getcsv(substr($row->Type, 4, -1), ',', "'");
                    $field = new Text($row->Field);
                    break;

                case 'smallint':
                    $field = (strpos($row->Type, 'unsigned') !== false) ?
                        new Integer($row->Field, 0, 65535) : new Integer($row->Field, -32768, 32767);
                    break;

                case 'text':
                    $field = new Text($row->Field);
                    break;

                case 'time':
                    $field = new Text($row->Field);
                    break;

                case 'timestamp':
                    $field = new DateTime($row->Field, 'Y-m-d H:i:s');
                    break;

                case 'tinyint':
                    $field = (strpos($row->Type, 'unsigned') !== false) ?
                        new Integer($row->Field, 0, 255) : new Integer($row->Field, -128, 127);
                    break;

                case 'varchar':
                    if (!preg_match('/\((\d+)\)/', $row->Type, $n)) {
                        throw new Exception("Unexpected format: {$row->Type}");
                    }
                    $field = new Text($row->Field, $n[1]);
                    break;

                case 'year':
                    if (!preg_match('/\((\d+)\)/', $row->Type, $n)) {
                        throw new Exception("Unexpected format: {$row->Type}");
                    }
                    $max_length = $n[1];
                    $field = new Integer($row->Field);
                    break;

                case 'tinyblob':
                case 'mediumblob':
                case 'blob':
                case 'longblob':
                    $field = new Blob($row->Field);
                    break;

                default:
                    throw new Exception("Unknown format: {$row->Type}");
            }

            $entity[$row->Field] = $field;
        }

        return $entity;
    }
}
