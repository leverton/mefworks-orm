<?php

require __DIR__ . '/../vendor/autoload.php';

$pdo = new PDO('sqlite::memory:');
$pdoDriver = new mef\Db\Driver\PdoDriver($pdo);
$writer = new mef\Sql\Writer\SqliteWriter($pdoDriver);

$db = new mef\Sql\Driver\SqlDriver($writer);

class UserModel extends mef\Orm\ActiveRecord
{
	protected function transformPassword($pwd)
	{
		$salt = base64_encode(pack('LS', mt_rand(0, 0xffffffff), mt_rand(0, 0xffff)));
		return crypt($pwd, '$1$'.$salt);
	}

	public function query()
	{
		return new UserQuery($this->orm);
	}
}

class UserSettingsModel extends mef\Orm\ActiveRecord
{

}

class RoleModel extends mef\Orm\ActiveRecord
{

}

class PrivilegeModel extends mef\Orm\ActiveRecord
{

}

$db->execute('CREATE TABLE user (id INTEGER PRIMARY KEY, first_name TEXT, last_name TEXT, password TEXT)');
$db->execute('CREATE TABLE user_role (id INTEGER PRIMARY KEY, user_id INTEGER, role_id INTEGER)');
$db->execute('CREATE TABLE user_settings (id INTEGER PRIMARY KEY, language TEXT)');

$db->execute('CREATE TABLE role (id INTEGER PRIMARY KEY, name TEXT)');
$db->execute('CREATE TABLE role_privilege (id INTEGER PRIMARY KEY, role_id INTEGER, privilege_id INTEGER)');
$db->execute('CREATE TABLE privilege (id INTEGER PRIMARY KEY, name TEXT)');


$orm = new mef\Orm\SQLManager($db);
$orm->lastInsertFunction = 'LAST_INSERT_ROWID';

$entity = new mef\Orm\Entity('user');
$entity['id'] = new mef\Orm\Integer('id');
$entity['first_name'] = new mef\Orm\Text('first_name');
$entity['last_name'] = new mef\Orm\Text('last_name');
$entity['password'] = new mef\Orm\Text('password');
$entity['latitude'] = new mef\Orm\FloatField('latitude');
$entity['longitude'] = new mef\Orm\FloatField('longitude');
$orm->mapEntity($entity, 'user', 'UserModel');

$entity = new mef\Orm\Entity('user_settings');
$entity['id'] = new mef\Orm\Integer('id');
$entity['language'] = new mef\Orm\Text('language');
$orm->mapEntity($entity, 'user_setting', 'UserSettingsModel');

$entity = new mef\Orm\Entity('role');
$entity['id'] = new mef\Orm\Integer('id');
$entity['name'] = new mef\Orm\Text('name');
$orm->mapEntity($entity, 'role', 'RoleModel');

$entity = new mef\Orm\Entity('privilege');
$entity['id'] = new mef\Orm\Integer('id');
$entity['name'] = new mef\Orm\Text('name');
$orm->mapEntity($entity, 'privilege', 'PrivilegeModel');

$user = $orm->create('user');
$user->first_name = 'John';
$user->last_name = 'Doe';
$user->save();
var_dump($user->asArray());

var_dump($orm->query('user')->where('last_name', '=', 'Doe')->findOne()->asArray());
